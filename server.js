'use strict';

const express = require('express');

// Constants
const PORT = process.env.PORT || 3030;

// App
const app = express();

app.get('/', (req, res) => {
  res.json({
    'message': 'Welcome to v2 of the api'
  }, 200);
});

app.get('/message', (req, res) => {
    res.json({
        'message': 'this is a new endpoint'
    }, 200);
});


app.get('/health', (req, res) => {
  res.json({
    message: 'api is heathy'
  }, 200);
});

app.listen(PORT, () => {
  console.log(`app started on port ${PORT}`)
});
